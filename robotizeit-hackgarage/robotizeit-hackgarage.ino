/*
    RobotizeIt.com garage door controller with SmartThings integration.
    Can control 2 garage doors using GPIO 12 and 13 as the relay pulse and
    uses GPIO 4 and 5 as a reed switch to detect when the door is closed.

    Implements OTA updates from Arduino IDE, ESP Smart Config for Network configuration,
    2 door control, and a timed door mode which allows you to only open the door a
    small amount based on the time you give the pulse.

    Uses a key for authenication that you pass on the URL line.  Should only be
    used on an encrypted network where you trust all parties.

    NO WARRANTY is provided and this code is for use expressly as a learning tool.
    RobotizeIt.com are not liable for any damage use of this may cause.
*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <TaskScheduler.h>
#include <NtpClientLib.h>
#include <ESP8266SSDP.h>
#include <EEPROM.h>
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

#define VERSION_STRING   "0.1.3"

//GPIO availiable are 2,4,5,16, 14(HSPI CLK), 12(HSPI_MSIO), 13(HSPI_MOSI)
//GPIO16 and 2 are used at reset and bootup
//
//Use GPIO 13 and 14 as inputs
//Use GPIO 4 and 5 as outputs
#define GARAGE_RELAY1_IO_NUM       12
#define GARAGE_RELAY2_IO_NUM       13
#define GARAGE_DOOR1_STATUS_IO_NUM 4
#define GARAGE_DOOR2_STATUS_IO_NUM 5

#define PULSE_LENGTH 500 //1000ms button pulse
#define KEYSETFLAG 0xA3

int reconnectcount = 0;

struct garage_saved_param {
  uint8_t door1_state;
  uint8_t door2_state;
  uint8_t req1_state;
  uint8_t req2_state;
  uint32_t door1_time;
  uint32_t door2_time;
  uint32_t req1_time;
  uint32_t req2_time;
  uint8_t  req1_pulseactive;
  uint8_t  req2_pulseactive;
  uint8_t numdoors;     //Define how many doors are active, can be 0, 1, or 2
  uint8_t pad;
};
struct garage_saved_param garage_param = {0};

WiFiEventHandler stationConnectedHandler;
WiFiEventHandler stationDisconnectedHandler;

Scheduler ts;
// Callbacks
void t1Callback();
void door1_push();
void door2_push();
void door1_release();
void door2_release();

//Tasks to handle the door pulses, these are created disabled
Task d1push(0, 1, &door1_push, &ts, false);
Task d2push(0, 1, &door2_push, &ts, false);
Task d1release(PULSE_LENGTH, 1, &door1_release, &ts, false);
Task d2release(PULSE_LENGTH, 1, &door2_release, &ts, false);
Task d1push_timed(0, 1, &door1_push, &ts, false);
Task d2push_timed(0, 1, &door2_push, &ts, false);
Task d1release_timed(PULSE_LENGTH, 1, &door1_release, &ts, false);
Task d2release_timed(PULSE_LENGTH, 1, &door2_release, &ts, false);

char *doorkey_default =  "123456789abcdefg";
String doorkey;

char *deviceType = "urn:robotizeit-com:device:GarageDoor:1";

#define HOSTNAME "hackgarage-" ///< Hostname. The setup function adds the Chip ID at the end.

// Create an instance of the server
// specify the port to listen on as an argument
ESP8266WebServer httpServer2(80);
ESP8266WebServer httpServer(8080);  //For firmware updates
// To upload through terminal you can use: curl -F "image=@firmware.bin" esp8266-webupdate.local:8080/update
ESP8266HTTPUpdateServer httpUpdater;

String getHackgarageVersion(void) {
  return VERSION_STRING;
}

String macToString(const unsigned char* mac) {
  char buf[20];
  snprintf(buf, sizeof(buf), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  return String(buf);
}

String getHostname(void) {
  return String(HOSTNAME + String(ESP.getChipId(), HEX));
}

char *get_door_status_string(void) {
  char *resp = (char *) new char[256];
  os_memset(resp, 0, 256);
  os_sprintf(resp,
             "{ \"doors\": [{ \"id\": 1, \"state\": %d, \"time\": %u }, { \"id\": 2, \"state\": %d, \"time\": %u, \"version\":\"%s\" }] }\r\n",
             user_garage_get_door1_status(), user_garage_get_door1_status_time(),
             user_garage_get_door2_status(), user_garage_get_door2_status_time(),
             getHackgarageVersion().c_str());
  return resp;
}

uint8 user_garage_get_door1_status(void)
{
  garage_param.door1_state = digitalRead(GARAGE_DOOR1_STATUS_IO_NUM);
  return garage_param.door1_state;
}

uint32 user_garage_get_door1_status_time(void)
{
  return garage_param.door1_time;
}

uint8 user_garage_get_door2_status(void)
{
  garage_param.door2_state = digitalRead(GARAGE_DOOR2_STATUS_IO_NUM);
  return garage_param.door2_state;
}

uint32 user_garage_get_door2_status_time(void)
{
  return garage_param.door2_time;
}

uint8 user_garage_get_door2_reqstatus(void)
{
  return garage_param.req2_state;
}

uint32 user_garage_get_door2_reqstatus_time(void)
{
  return garage_param.req2_time;
}

uint8 user_garage_get_door1_reqstatus(void)
{
  return garage_param.req1_state;
}

uint32 user_garage_get_door1_reqstatus_time(void)
{
  return garage_param.req1_time;
}

uint8 user_garage_get_numdoors(void)
{
  return garage_param.numdoors;
}

void getDoorStates(void) {
  garage_param.door1_state = digitalRead(GARAGE_DOOR1_STATUS_IO_NUM);
  garage_param.door2_state = digitalRead(GARAGE_DOOR2_STATUS_IO_NUM);
}

/******************************************************************************
   FunctionName : user_garage_set_status
   Description  : set plug's status, 0x00 or 0x01
   Parameters   : uint8 - status
   Returns      : none
*******************************************************************************/
void user_garage_set_status(bool status, int doornum)
{
  if (doornum == 1)
    if (status != garage_param.door1_state) {
      if (status > 1) {
        //os_printf("error status input!\n");
        return;
      }

      garage_param.req1_state = status;
      //      GARAGE_STATUS_OUTPUT(GARAGE_RELAY1_IO_NUM, status);
    }
    else {
      if (status != garage_param.door2_state) {
        if (status > 1) {
          //os_printf("error status input!\n");
          return;
        }

        garage_param.req2_state = status;
        //            GARAGE_STATUS_OUTPUT(GARAGE_RELAY2_IO_NUM, status);
      }
    }
}

void door1_push(void) {
  digitalWrite(GARAGE_RELAY1_IO_NUM, 1);
}

void door1_release(void) {
  digitalWrite(GARAGE_RELAY1_IO_NUM, 0);
}

void door2_push(void) {
  digitalWrite(GARAGE_RELAY2_IO_NUM, 1);
}

void door2_release(void) {
  digitalWrite(GARAGE_RELAY2_IO_NUM, 0);
}

void user_garage_open_door1(int timeout)
{

  if (user_garage_get_door1_status() == 0) {
    //.5 second pulse to open the door
    garage_param.req1_time = sntp_get_current_timestamp();
    garage_param.req1_pulseactive = 1;
    garage_param.req1_state = 1;

    //Setup the task chain to accomplish the requested action
    d1push.restart();
    d1release.restartDelayed();
    if (timeout > 0) {
      d1push_timed.restartDelayed((timeout * 1000));
      d1release_timed.restartDelayed((timeout * 1000) + PULSE_LENGTH);
    }
  }

}

void user_garage_close_door1(int timeout)
{

  if (user_garage_get_door1_status() == 1) {
    garage_param.req1_time = sntp_get_current_timestamp();
    //.5 second pulse to open the door
    garage_param.req1_state = 0;
    garage_param.req1_pulseactive = 1;

    //Setup the task chain to accomplish the requested action
    d1push.restart();
    d1release.restartDelayed();
    if (timeout > 0) {
      d1push_timed.restartDelayed((timeout * 1000));
      d1release_timed.restartDelayed((timeout * 1000) + PULSE_LENGTH);
    }
  }
}

void user_garage_open_door2(int timeout)
{

  if (user_garage_get_door2_status() == 0) {
    garage_param.req2_time = sntp_get_current_timestamp();
    garage_param.req2_state = 1;
    garage_param.req2_pulseactive = 1;

    //Setup the task chain to accomplish the requested action
    d2push.restart();
    d2release.restartDelayed();
    if (timeout > 0) {
      d2push_timed.restartDelayed((timeout * 1000));
      d2release_timed.restartDelayed((timeout * 1000) + PULSE_LENGTH);
    }
  }
}

void user_garage_close_door2(int timeout)
{

  if (user_garage_get_door2_status() == 1) {
    garage_param.req2_time = sntp_get_current_timestamp();
    //.5 second pulse to open the door
    garage_param.req2_state = 0;
    garage_param.req2_pulseactive = 1;

    //Setup the task chain to accomplish the requested action
    d2push.restart();
    d2release.restartDelayed();
    if (timeout > 0) {
      d2push_timed.restartDelayed((timeout * 1000));
      d2release_timed.restartDelayed((timeout * 1000) + PULSE_LENGTH);
    }
  }
}

void onSTAGotIP(WiFiEventStationModeGotIP ipInfo) {
  Serial.printf("Got IP: %s\r\n", ipInfo.ip.toString().c_str());
  NTP.begin("pool.ntp.org", 1, true);
  NTP.setInterval(63);
  digitalWrite(2, LOW);
}

void onSTADisconnected(WiFiEventStationModeDisconnected event_info) {
  Serial.printf("Disconnected from SSID: %s\n", event_info.ssid.c_str());
  Serial.printf("Reason: %d\n", event_info.reason);
  digitalWrite(2, HIGH);
}

void printFlashInfo(void) {
  uint32_t realSize = ESP.getFlashChipRealSize();
  uint32_t ideSize = ESP.getFlashChipSize();
  FlashMode_t ideMode = ESP.getFlashChipMode();

  Serial.printf("Flash real id:   %08X\n", ESP.getFlashChipId());
  Serial.printf("Flash real size: %u\n\n", realSize);

  Serial.printf("Flash ide  size: %u\n", ideSize);
  Serial.printf("Flash ide speed: %u\n", ESP.getFlashChipSpeed());
  Serial.printf("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));

  Serial.printf("Hack Garage v%s running...\n", VERSION_STRING);
}

char * getHackgarageInfo(void) {
  String data;

  long rssi = WiFi.RSSI();
  uint32_t realSize = ESP.getFlashChipRealSize();
  uint32_t ideSize = ESP.getFlashChipSize();
  FlashMode_t ideMode = ESP.getFlashChipMode();

  char *resp = (char *) new char[256];
  os_memset(resp, 0, 256);
  os_sprintf(resp,
             "{ \"flash size\": \"%d\", \"reconnects\": \"%d\", \"rssi\": \"%d\" }\r\n", realSize, reconnectcount, rssi);
  return resp;  //user must free.

}


String setupHostname(void) {
  // Set Hostname.
  String hostname = getHostname();
  WiFi.hostname(hostname);

  // Print hostname.
  Serial.println("Hostname: " + hostname);
  //Serial.println(WiFi.hostname());
  return hostname;
}

bool setupSSDP(void) {
  Serial.printf("Starting SSDP...\n");
  SSDP.setSchemaURL("ssdp/schema.xml");
  SSDP.setHTTPPort(80);
  SSDP.setName(getHostname());
  SSDP.setSerialNumber(ESP.getChipId());
  SSDP.setURL("info.html");
  SSDP.setModelName("hackgarage2");
  SSDP.setModelNumber("GD2");
  SSDP.setModelURL("http://www.robotizeit.com");
  SSDP.setManufacturer("RobotizeIt.com");
  SSDP.setManufacturerURL("http://www.robotizeit.com");
  SSDP.setDeviceType(deviceType);
  SSDP.begin();
}

bool setnewkey(String newkey) {
    Serial.println("writing eeprom userkey:");
    for (int i = 4; i < 4+24; ++i)
    {
              EEPROM.write(i, 0);
    }
    EEPROM.commit();
    for (int i = 4; i < 4+newkey.length(); ++i)
    {
              EEPROM.write(i, newkey[i-4]);
    }
    EEPROM.write(0, KEYSETFLAG);  //Magic to indicate the key is set
    Serial.print("Wrote: ");
    Serial.println(newkey);
    EEPROM.commit();
    return true;
}

void handlePutUpdateKey() {
  String newkey = httpServer2.arg("authkey");
  String tinfo;
  if (newkey == String(doorkey) && newkey.length() <= 24 ) {
    Serial.printf("authkey match - changing!\n");
    setnewkey(newkey);
    doorkey = newkey;
    tinfo = "{ \"result\": \"key update successful\"}\r\n";
  } else {
    Serial.printf("Failed to change authkey\n", httpServer2.arg("authkey").c_str() );
    tinfo =  "{ \"result\": \"key update failed\"}\r\n";
  }
  httpServer2.send ( sizeof(tinfo), "text/json", tinfo );
}

void handlePutOpenDoor1() {
  int timeout = 0;
  char *tinfo = get_door_status_string();

  if (httpServer2.hasArg("time")) {
    timeout = httpServer2.arg("time").toInt();
  }

  if (httpServer2.arg("authkey") == String(doorkey)) {
    Serial.printf("Door 1 authkey match!\n");
    user_garage_open_door1(timeout);
  } else {
    Serial.printf("Door 1 open authkey not matched %s\n", httpServer2.arg("authkey").c_str() );
  }

  httpServer2.send ( sizeof(tinfo), "text/json", tinfo );
  delete [] tinfo;
}

void handlePutOpenDoor2() {
  int timeout = 0;
  char *tinfo = get_door_status_string();

  if (httpServer2.hasArg("time")) {
    timeout = httpServer2.arg("time").toInt();
  }

  if (httpServer2.arg("authkey") == String(doorkey)) {
    Serial.printf("Door 2 authkey match!\n");
    user_garage_open_door2(timeout);
  } else {
    Serial.printf("Door 2 open authkey not matched %s\n", httpServer2.arg("authkey").c_str() );
  }

  httpServer2.send ( sizeof(tinfo), "text/json", tinfo );
  delete [] tinfo;
}

void handlePutCloseDoor1() {
  int timeout = 0;
  char *tinfo = get_door_status_string();

  if (httpServer2.hasArg("time")) {
    timeout = httpServer2.arg("time").toInt();
  }

  if (httpServer2.arg("authkey") == String(doorkey)) {
    Serial.printf("Door 1 close authkey match!\n");
    user_garage_close_door1(timeout);
  } else {
    Serial.printf("Door 1 close authkey not matched %s\n", httpServer2.arg("authkey").c_str() );
  }

  httpServer2.send ( sizeof(tinfo), "text/json", tinfo );
  delete [] tinfo;
}

void handlePutCloseDoor2() {
  int timeout = 0;
  char *tinfo = get_door_status_string();

  if (httpServer2.hasArg("time")) {
    timeout = httpServer2.arg("time").toInt();
  }

  if (httpServer2.arg("authkey") == String(doorkey)) {
    Serial.printf("Door 2 close authkey match!\n");
    user_garage_close_door2(timeout);
  } else {
    Serial.printf("Door 2 close authkey not matched %s\n", httpServer2.arg("authkey").c_str() );
  }

  httpServer2.send ( sizeof(tinfo), "text/json", tinfo );
  delete [] tinfo;
}

void getDeviceInfo() {
  char *resp = (char *) new char[256];
  os_memset(resp, 0, 256);
  os_sprintf(resp,
             "{ \"device\": \"%s\", \"model\": \"%s\", \"vendor\": \"%s\" }\r\n", getHostname().c_str(), "hackgarage", "robotizeIt.com");
}

void getDetails() {
  const char * tinfo = getHackgarageInfo();
  httpServer2.send ( sizeof(tinfo), "text/json", tinfo);
  delete [] tinfo;
}

void getInfo() {
  char *tinfo = get_door_status_string();
  httpServer2.send ( sizeof(tinfo), "text/json", tinfo );
  delete [] tinfo;
}

void getSchema() {
  char temp[400];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  snprintf ( temp, 400,

             "{\
  \"device\":\
  {\
  \"name\": \"%s\",\
  \"UDN\": \"%s\", \
  \"serialNumber\": \"%x\",\
  \"uptime\": \"%02d:%02d:%02d\"\
  }\
}\n\n",

             getHostname().c_str(), deviceType, ESP.getChipId(), hr, min % 60, sec % 60
           );

  httpServer2.send ( 400, "application/json", temp );
}

void handleRoot() {
  char temp[1024];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  snprintf ( temp, 400,

             "<html>\
  <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>RobotizeIt.com Hackgarage</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>Device name is %s</h1>\
    <p>To use this device see the blog entry at <a href=\"http://www.robotizeit.com\">RobotizeIt</a></p>\
    <p>Uptime: %02d:%02d:%02d</p>\
  </body>\
</html>",

  getHostname().c_str(), hr, min % 60, sec % 60 );

  httpServer2.send ( strlen(temp), "text/html", temp );
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += httpServer.uri();
  message += "\nMethod: ";
  message += ( httpServer.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += httpServer.args();
  message += "\n";

  for ( uint8_t i = 0; i < httpServer.args(); i++ ) {
    message += " " + httpServer.argName ( i ) + ": " + httpServer.arg ( i ) + "\n";
  }

  httpServer.send ( 404, "text/plain", message );
}

void handleNotFound2() {
  String message = "Unable to locate requested resource.\n\n";
  message += "URI: ";
  message += httpServer.uri();
  message += "\nMethod: ";
  message += ( httpServer.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += httpServer.args();
  message += "\n";

  for ( uint8_t i = 0; i < httpServer.args(); i++ ) {
    message += " " + httpServer.argName ( i ) + ": " + httpServer.arg ( i ) + "\n";
  }

  httpServer2.send ( 404, "text/plain", message );
}

void onStationConnected(const WiFiEventSoftAPModeStationConnected& evt) {
  Serial.print("Station connected: ");
  Serial.println(macToString(evt.mac));
  reconnectcount++;
}

void onStationDisconnected(const WiFiEventSoftAPModeStationDisconnected& evt) {
  Serial.print("Station disconnected: ");
  Serial.println(macToString(evt.mac));

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

}

void setup() {
  static WiFiEventHandler e1, e2;

  pinMode(GARAGE_RELAY1_IO_NUM, OUTPUT); //Door 1 Control Output
  pinMode(GARAGE_RELAY2_IO_NUM, OUTPUT); //Door 2 Control Output
  pinMode(GARAGE_DOOR1_STATUS_IO_NUM, INPUT_PULLUP); //Door 1 Sensor Input
  pinMode(GARAGE_DOOR2_STATUS_IO_NUM, INPUT_PULLUP); //Door 2 Sensor Input

  Serial.begin(115200);
  delay(15);

  EEPROM.begin(256); //To save some data

  printFlashInfo();

  Serial.println("Reading EEPROM userkey");
  byte ukset = byte(EEPROM.read(0));
  if(ukset != KEYSETFLAG) {
    doorkey = String(doorkey_default);
    setnewkey(doorkey);
  } else {
    String tmpkey;
    for (int i = 4; i < 24+4; ++i)
    {
      tmpkey += char(EEPROM.read(i));
    }
    doorkey = tmpkey;
    Serial.print("DoorKey: ");
    Serial.print(doorkey);
    Serial.print(" set: ");
    Serial.println(ukset);
  }

  WiFiManager wifiManager;
  //wifiManager.resetSettings();  //For testing only
  EEPROM.write(0, 0);
  EEPROM.commit();

  wifiManager.setTimeout(300);

  if(!wifiManager.autoConnect(getHostname().c_str())) {
    Serial.println("failed to connect and hit timeout");
    delay(200);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }


//  //Try smartconfig
//  WiFi.mode(WIFI_AP_STA);
//  WiFi.beginSmartConfig();
//
//  /* Wait for SmartConfig packet from mobile */
//  int starttime = millis();
//  Serial.println("Waiting for SmartConfig.");
//  while (!WiFi.smartConfigDone()) {
//    int elapsed = millis() - starttime;
//    if (elapsed > 20000)  {
//      WiFi.stopSmartConfig();
//      Serial.println("\nSmart Config Failed.");
//      break;
//    }
//    delay(500);
//    Serial.print(".");
//  }
//  Serial.println("");
//  Serial.println("SmartConfig done.");
//
//  /* Wait for WiFi to connect to AP */
//  Serial.println("Waiting for WiFi");
//  int waitcount = 0;
//  while (WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    waitcount++;
//    Serial.print(".");
//    if(waitcount > 120) {  //Reset and try again
//      ESP.restart();
//    }
//  }


  Serial.println("WiFi Connected.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  WiFi.setAutoReconnect(true);

  //Get the time
  NTP.onNTPSyncEvent([](NTPSyncEvent_t ntpEvent) {
    if (ntpEvent) {
      Serial.print("Time Sync error: ");
      if (ntpEvent == noResponse)
        Serial.println("NTP server not reachable");
      else if (ntpEvent == invalidAddress)
        Serial.println("Invalid NTP server address");
    }
    else {
      Serial.print("Got NTP time: ");
      Serial.println(NTP.getTimeDateString(NTP.getLastNTPSync()));
    }
  });

  //WiFi.onEvent([](WiFiEvent_t e) {
  //  Serial.printf("Event wifi -----> %d\n", e);
  //});
  e1 = WiFi.onStationModeGotIP(onSTAGotIP);// As soon WiFi is connected, start NTP Client
  e2 = WiFi.onStationModeDisconnected(onSTADisconnected);

  // Print the IP address
  Serial.println(WiFi.localIP());
  String hostname = setupHostname();

  // Start OTA server.
  ArduinoOTA.onStart([]() {
    Serial.println("OTA Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nOTA End");
  });

  //ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
  //  Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
  //});
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("OTA Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.setHostname((const char *)hostname.c_str());
  ArduinoOTA.begin();

  //Start SSDP
  setupSSDP();

  httpUpdater.setup(&httpServer);

  httpServer2.on ( "/", handleRoot );
  httpServer2.on ( "/info", getInfo );
  httpServer2.on ( "/details", getDetails );
  httpServer2.on ( "/v1/garage/info",  getInfo );
  httpServer2.on ( "/info.html",       getInfo );
  httpServer2.on ( "/ssdp/schema.xml", getSchema );

  httpServer2.on ( "/v1/updatekey",      HTTP_PUT, handlePutUpdateKey );

  httpServer2.on ( "/v1/garage/1/close", HTTP_GET, getInfo );
  httpServer2.on ( "/v1/garage/1/open",  HTTP_GET, getInfo );
  httpServer2.on ( "/v1/garage/2/close", HTTP_GET, getInfo );
  httpServer2.on ( "/v1/garage/2/open",  HTTP_GET, getInfo );

  httpServer2.on ( "/v1/garage/1/close", HTTP_PUT, handlePutCloseDoor1 );
  httpServer2.on ( "/v1/garage/1/open",  HTTP_PUT, handlePutOpenDoor1 );
  httpServer2.on ( "/v1/garage/2/close", HTTP_PUT, handlePutCloseDoor2 );
  httpServer2.on ( "/v1/garage/2/open",  HTTP_PUT, handlePutOpenDoor2 );

  httpServer.onNotFound ( handleNotFound );
  httpServer2.onNotFound ( handleNotFound2 );
  httpServer2.begin();
  httpServer.begin();

  MDNS.addService("http", "tcp", 8080);
  MDNS.addService("http", "tcp", 80);
  Serial.printf("HTTPUpdateServer ready! Open http://%s.local:8080/update in your browser\n", hostname.c_str());
  Serial.printf("Sketch size : %u\n", ESP.getSketchSize());
  Serial.printf("Free size : %u\n", ESP.getFreeSketchSpace());

}

void loop() {
  ts.execute();
  ArduinoOTA.handle();
  httpServer.handleClient();
  httpServer2.handleClient();
}
