/**
 *  RobotizeIt HackGarage Connect Virtual
 *
 *  Copyright 2017 RobotizeIt.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 */
 import groovy.json.JsonSlurper

definition (
    name: "RobotizeIt HackGarage Connect Virtual",
    namespace: "robotizeit",
    author: "robotizeit",
    description: "Connect App for the RobotizeIt Garage Opener",
	category: "Convenience",
    iconUrl: "http://cdn.device-icons.smartthings.com/Home/home2-icn.png",
    iconX2Url: "http://cdn.device-icons.smartthings.com/Home/home2-icn@2x.png",
    iconX3Url: "http://cdn.device-icons.smartthings.com/Home/home2-icn@2x.png")
{
	appSetting "myIp"
    appSetting "myPort"
    appSetting "myMac"
}

preferences {

  page(name: "deviceSetup", title: "RobotizeIt Garage Settings", nextPage: "deviceDiscovery") {
    section("RobotizeIt HackGarage Setup"){
//    input "deviceIP", "text", "title": "HackGarage IP", multiple: false, required: true
//    input "devicePort", "text", "title": "HackGarage Port", multiple: false, required: true
     input "numberofdoors", type: "number", title: "Number of Doors:", range: "1..2", required: true
     input "deviceKey", "text", "title": "HackGarage Key", multiple: false, required: true
     input "myHub", "hub", title: "On which hub?", multiple: false, required: true
    }
  }

    page(name: "deviceDiscovery", title: "RobotizeIt Garage Device Setup", content: "deviceDiscovery")
}


def deviceDiscovery() {
    log.debug("starting device discovery")
	def options = [:]
	def devices = getVerifiedDevices()

    log.debug("starting device discovery devices ${devices}")
    if(devices.size() == 0) {
    	log.debug("No devices, subscribing to ssdp ${devices.size()}")
    	ssdpSubscribe()

		ssdpDiscover()
		verifyDevices()
    } else {
		devices.each {
			def value = it.value.name ?: "UPnP Device ${it.value.ssdpUSN.split(':')[1][-3..-1]}"
			def key = it.value.mac
			options["${key}"] = value
		}
	}

	return dynamicPage(name: "deviceDiscovery", title: "Discovery Started!", nextPage: "", refreshInterval: 20, install: true, uninstall: true) {
		section("Please wait while we discover your RobotizeIt Garage Device. Discovery can take five minutes or more, so sit back and relax! Select your device below once discovered.") {
			input "selectedDevices", "enum", required: false, title: "Select Devices (${options.size() ?: 0} found)", multiple: true, options: options
		}
	}
}

def installed() {
  log.debug "Installed with settings: ${settings}"

  initialize()
}

def initialize() {

	log.debug("Initialize Callded")

    if(!state.discoveredIp) {
		state.discoveredIp = ""
    }
    if(!state.discoveredPort) {
		state.discoveredPort = ""
    }
    if(!state.discoveredMac) {
		state.discoveredMac = ""
    }

	unsubscribe()
	unschedule()

	ssdpSubscribe()

	if (selectedDevices) {
		addGarageDevices()
	}

    runEvery5Minutes("ssdpDiscover")
}

def updated() {
	log.debug "Updated with settings: ${settings}"

	unsubscribe()
	initialize()
    ssdpSubscribe()
}

void ssdpDiscover() {
	ssdpSubscribe()
    log.debug("ssdpDiscover run ${getURN()}")
    sendHubCommand(new physicalgraph.device.HubAction("lan discovery ${getURN()}", physicalgraph.device.Protocol.LAN))
    sendHubCommand(new physicalgraph.device.HubAction("lan discover mdns/dns-sd _hackgarage._tcp", physicalgraph.device.Protocol.LAN))
}

void ssdpSubscribe() {
	subscribe(location, "ssdpTerm.${getURN()}", ssdpHandler)
}

def getVerifiedDevices() {
	getDevices().findAll{ it.value.verified == true }
}

def getDevices() {
	if (!state.devices) {
		state.devices = [:]
	}
	state.devices
}

Map verifiedDevices() {
	def devices = getVerifiedDevices()
	def map = [:]
	devices.each {
		def value = it.value.name ?: "UPnP Device ${it.value.ssdpUSN.split(':')[1][-3..-1]}"
		def key = it.value.mac
		map["${key}"] = value
	}
	map
}

void verifyDevices() {
	log.debug "verifying devices"
	def devices = getDevices().findAll { it?.value?.verified != true }
    if(devices != null) {
    	log.debug "verifying devices ${devices}"
		devices.each {
        	if(it.value.networkAddress) {
				int port = convertHexToInt(it.value.deviceAddress)
				String ip = convertHexToIP(it.value.networkAddress)
				String host = "${ip}:${port}"
        		log.debug "verifying device ${it.value.ssdpPath} host: ${host} mac: ${it.value.mac}"
				sendHubCommand(new physicalgraph.device.HubAction("""GET ${it.value.ssdpPath} HTTP/1.1\r\nHOST: $host\r\n\r\n""", physicalgraph.device.Protocol.LAN, host, [callback: deviceDescriptionHandler]))
			}
        }
    }
}

def parse(String description){
	log.debug("Parse callback called")
	def msg = parseLanMessage(description)
	log.debug "msg: ${msg}"
	log.debug "body: ${msg.body}"
	log.debug "xml: ${msg.xml}"
}

def ssdpHandler(evt) {
	log.debug("ssdpHandler event ${evt.description}")
    def description = evt.description
    def hub = evt?.hubId

    def parsedEvent = parseLanMessage(description)
    parsedEvent << ["hub":hub]

    def devices = getDevices()
    String ssdpUSN = parsedEvent.ssdpUSN.toString()
    log.debug("ssdpHandler ssdpUSN is: ${ssdpUSN} devices ${devices}")
    def mymac = parsedEvent.mac.toString()

    if(!ssdpUSN) {
        log.debug "SSDPUSN null, returning"
    	return
    }

    if(mymac == "000000000000") {
     	log.debug "MAC null, returning"
    	return
    }

    //This populates the IP before the devices have been added to the
    state.discoveredIp = parsedEvent.networkAddress
    state.discoveredPort = parsedEvent.deviceAddress
    state.discoveredMac = mymac
    appSettings.myIp = parsedEvent.networkAddress
    appSettings.myPort = parsedEvent.deviceAddress

    log.debug("Devices size ${devices.size()}")

    getChildDevices().each {
    	log.debug("Child device found ${it} with id ${it.deviceNetworkId} - Subscribing to events")
        subscribe(it, "switch", switchChange)
    }

    if (devices."${ssdpUSN}") {
        def d = devices."${ssdpUSN}"

        log.debug("Found Top device ${ssdpUSN} d.ip ${d.ip} pe.ip ${parsedEvent.ip} d.port ${d.port} pe.port ${parsedEvent.port}")
    } else {
        devices << ["${ssdpUSN}": parsedEvent]
    }
}

void deviceDescriptionHandler(physicalgraph.device.HubResponse hubResponse) {
	def body = hubResponse.json
    log.debug "deviceDescriptionHandler body json: ${body}"
	def devices = getDevices()
    log.debug "deviceDescriptionHandler devices: ${devices}"
    def device = devices.find { it?.key?.contains(body?.device?.UDN) }
	if (device) {
        //Save verified devices IP
        state.discoveredIp = body?.device?.networkAddress
        state.discoveredPort = body?.device?.deviceAddress
        state.discoveredMac = body?.device?.mac
    	log.debug "deviceDescriptionHandler device: ${device} mac ${state.discoveredMac} ip ${state.discoveredIp}"
        log.debug "deviceDescriptionHandler device: ${body?.device?.name} ${body?.device?.UDN}"
        device.value << [name: "hackgarage", model:body?.device?.name, serialNumber:body?.device?.UDN, verified: true]
	}
}

//def addGarageDevices(deviceName, deviceType, deviceConfig) {
def addGarageDevices() {
    //Installed with settings: [numberofdoors:2, deviceKey:xp3hgh3WZSFnUOFF, myHub:Home Hub, selectedDevices:[A020A6063DD0]]

    def theDeviceNetworkId = settings.selectedDevices[0];
    def deviceName = "hackgarge"

    log.trace "adding garage devices: $theDeviceNetworkId";

  	def theDevice = getChildDevices().find{ d -> d.deviceNetworkId }

    if(theDevice){ // The switch already exists
    	log.debug "Found existing device with networkid ${theDevice.deviceNetworkId} - subscribing to events"

        subscribe(theDevice, "switch", switchChange)

    } else { // The switch does not exist
    	def numdoors = numberofdoors as Integer
    	if(deviceName){ // The user filled in data about this switch
        	1.upto(numdoors, {
    			log.debug "This device does not exist, creating a new one now ${theDeviceNetworkId}-${it}"
        		def d = addChildDevice("robotizeit", "Virtual Hackgarage", "${theDeviceNetworkId}-${it}", myHub.id,
                [label:"${deviceName}-${it}", name:"${deviceName}-${it}", doornum:it, "data": ["doornum":it] ])
	    		subscribe(d, "switch", switchChange)
            })
       	}
    }

}

def uninstalled() {
  def delete = getChildDevices()
    delete.each {
    	log.trace "about to delete device"
        deleteChildDevice(it.deviceNetworkId)
    }
}

def cmdResponse(physicalgraph.device.HubResponse hubResponse){
  log.debug "Hubaction Response ${hubResponse}"
  def msg = parseLanMessage(hubResponse.description);
  log.debug "Hubaction Response Description ${msg.body}"
  def body = msg.body
  log.debug "cmdResponse body json: ${body}"
  log.debug "Parsing '${body}'"

    def slurper = new JsonSlurper()
    def jsnresult = slurper.parseText(body)

    def myindex = 1
    def dn = numberofdoors as Integer

    1.upto(dn, {
    	if(jsnresult.doors[it-1].state == 1) {
    		log.debug "door ${it} is open: ${jsnresult.doors[it-1]}"
        	sendEvent(name: "door", value: "open", isStateChange: true)
            def childNetworkId = "${state.discoveredMac}-${it}"
            //def theDevice = getChildDevices().find{ d -> d.deviceNetworkId.startsWith(childNetworkId) }
            def theDevice = getChildDevices().find{ it.deviceNetworkId == childNetworkId }

            if(theDevice) {
                theDevice.setDoorState("open");
                log.trace "$theDevice set to open"
            }
    	} else {
    		log.debug "door ${it} is closed: ${jsnresult.doors[it-1]}"
        	sendEvent(name: "door", value: "closed", isStateChange: true)
            def childNetworkId = "${state.discoveredMac}-${it}"
            //def theDevice = getChildDevices().find{ d -> d.deviceNetworkId.startsWith(childNetworkId) }
            def theDevice = getChildDevices().find{ it.deviceNetworkId == childNetworkId }

            if(theDevice) {
                theDevice.setDoorState("closed");
                log.trace "$theDevice set to closed"
            }
    	}
    })
}

def updateRelayDevice(GPIO, state, childDevices) {

  	def theSwitch = childDevices.find{ d -> d.deviceNetworkId.endsWith(".$GPIO") }
    if(theSwitch) {
    	log.debug "Updating switch $theSwitch for GPIO $GPIO with value $state"
        theSwitch.changeSwitchState(state)
    }
}

def switchChange(evt){

	log.debug "Switch event! ${evt}";
    log.debug evt.value;
    log.debug "switchChange data ${evt.data}"
    if(evt.value == "on" || evt.value == "off" || evt.value == "open" || evt.value == "close") return;

    def data = parseJson(evt.data)
    def doornum = data.doornum
    def eventcmd = data.command

    log.debug "Doornum is ${doornum} event is ${eventcmd}" ;

    runCmd(eventcmd, doornum as Integer)

    setDeviceState(doornum, state);

    return;
}

def setDeviceState(doornum, state) {
	log.debug "Executing setDeviceState for door ${doornum}"

    // Determine the path to post which will set the switch to the desired state
    def Path = "/GPIO/" + gpio + "/value/";
	Path += (state == "on") ? "1" : "0";

    //runCmd("open")
    //executeRequest(Path, "POST", true, gpio);
}

def runCmd(String varCommand, Integer doornumber) {
    log.debug("runCmd ${varCommand} doornum ${doornumber}")
    //log.debug("ip4 ${getDataValue("ip")}")

    //if(getDeviceAddress == NULL) {
    //	log.debug("trying to find IP address")
	//}

    def hosthex = state.discoveredIp
	def porthex = state.discoveredPort
    def machex = state.discoveredMac

    //From UPNP
	//def hosthex = getDataValue("ip")
	//def porthex = getDataValue("port")

    //From form in device type
    if(hosthex == NULL)
    	hosthex = convertIPtoHex(getDeviceAddress()).toUpperCase()
    if(porthex == NULL)
		porthex = convertPortToHex(getDevicePort()).toUpperCase()

	def dn = doornumber as Integer
    def authkey = deviceKey
    def DevicePath = ""
    def method = "GET"

    if (varCommand == "open") {
    	DevicePath = "/v1/garage/${dn}/open?authkey=$authkey"
        method = "PUT"
    } else if (varCommand == "close") {
    	DevicePath = "/v1/garage/${dn}/close?authkey=$authkey"
        method = "PUT"
    } else {  //Assume refresh
    	DevicePath = "/v1/garage/info?authkey=$authkey"
        method = "GET"
    }

    def path = DevicePath
	log.debug "path is: $path"
	//def body = ""//varCommand
	//log.debug "body is: $body"

	//def headers = [HOST:"${hosthex}:${porthex}"]
    int port = convertHexToInt(porthex)
    String ip = convertHexToIP(hosthex)
    String myhost = "${ip}:${port}"
	//def headers = [host:"${convertHexToIP(hosthex)}:${convertHexToInt(porthex)}"]
    def headers = [host:"${convertHexToIP(hosthex)}"]
    //headers.put("Content-Type", "text/html")
	//headers.put("Content-Type", "application/x-www-form-urlencoded")

	log.debug "The Header is $headers myhost ${myhost} method ${method} path ${path} mac ${machex}"
	//log.debug "The method is $method"

	try {
		sendHubCommand(new physicalgraph.device.HubAction("""${method} ${path} HTTP/1.1\r\nHOST: $myhost\r\n\r\n""", physicalgraph.device.Protocol.LAN, machex, [callback: cmdResponse]))
    }
	catch (Exception e) {
		log.debug "Hit Exception $e on $hubAction"
	}
}


/* Helper functions to get the network device ID */
private String NetworkDeviceId(){
    def iphex = convertIPtoHex(settings.piIP).toUpperCase()
    def porthex = convertPortToHex(settings.piPort)
    return "$iphex:$porthex"
}

private Integer convertHexToInt(hex) {
	Integer.parseInt(hex,16)
}

private String convertHexToIP(hex) {
	[convertHexToInt(hex[0..1]),convertHexToInt(hex[2..3]),convertHexToInt(hex[4..5]),convertHexToInt(hex[6..7])].join(".")
}

private String convertIPtoHex(ipAddress) {
    String hex = ipAddress.tokenize( '.' ).collect {  String.format( '%02x', it.toInteger() ) }.join()
    //log.debug "IP address entered is $ipAddress and the converted hex code is $hex"
    return hex
}

private String convertPortToHex(port) {
    String hexport = port.toString().format( '%04x', port.toInteger() )
    //log.debug hexport
    return hexport
}

private String getURN() {
	"urn:robotizeit-com:device:GarageDoor:1"
}
