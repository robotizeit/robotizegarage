/**
 *  Hackgarage virtual device
 *
 *  Copyright 2017 RobotizeIt.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 */

metadata {
	definition (name: "Virtual Hackgarage", namespace: "robotizeit", author: "robotizeit") {
        capability "Actuator"
		capability "Door Control"
		capability "Garage Door Control"
		capability "Contact Sensor"
        capability "Switch"
		capability "Refresh"
		capability "Sensor"
        capability "Polling"

		attribute "opener", "string"
        attribute "doorstates", "string"
		attribute "doornumber", "number"

        command "changeSwitchState", ["string"]
		command "DoorOpener"
	}

	simulator {
		// TODO: define status and reply messages here
	}

	tiles {

        standardTile("DoorOpener", "device.door", width: 3, height: 3, canChangeIcon: true, canChangeBackground: true) {
			state "closed", label:'CLOSED' , action: "open", icon: "st.doors.garage.garage-closed", backgroundColor:"#ffffff", nextState: "opening"
			state "open", label: 'OPEN', action: "close", icon: "st.doors.garage.garage-open", backgroundColor: "#79b821", nextState: "closing"
            state "unknown", label:'UNKNOWN' , action: "", icon: "st.doors.garage.garage-unknown", backgroundColor:"#FFAA33"
			state "opening", label: 'OPENING', action: "", icon: "st.doors.garage.garage-opening", backgroundColor: "#FFAA33"
            state "closing", label: 'CLOSING', action: "", icon: "st.doors.garage.garage-closing", backgroundColor: "#FFAA33"
		}

        standardTile("refresh", "device.door", inactiveLabel: false, decoration: "flat") {
			state "default", label:'', action:"refresh", icon:"st.secondary.refresh"
		}

		main "DoorOpener"
		details(["DoorOpener", "refresh"])
	}
}

// parse events into attributes
def parse(String description) {
	log.debug "Virtual siwtch parsing '${description}'"
}

def poll() {
	log.debug "Executing 'poll'"

    def lastState = device.currentValue("switch")
    def doornum = getDataValue("doornum")

    log.debug "Poll with state ${lastState} doornum ${doornum}"

    sendEvent(name: "switch", value: device.deviceNetworkId, data:[command:"refresh", doornum:doornum])
    sendEvent(name: "switch", value: "refresh", data:[command:"refresh", doornum:doornum]);
    runIn(120, poll)
}

def refresh() {
	log.debug "Executing 'refresh'"

	poll();
}

def open() {
	log.debug "Executing 'open'"

    def doornum = getDataValue("doornum")
    sendEvent(name: "switch", value: device.deviceNetworkId + ".open", data:[command:"open", doornum:doornum]);
    sendEvent(name: "switch", value: "open", data:[command:"open", doornum:doornum]);
    runIn(10, poll)
}

def close() {
	log.debug "Executing 'close'"

    def doornum = getDataValue("doornum")
	sendEvent(name: "switch", value: device.deviceNetworkId + ".close", data:[command:"close", doornum:doornum]);
    sendEvent(name: "switch", value: "close", data:[command:"close", doornum:doornum]);
}

//Called by parent to set the state of the door
def setDoorState(newState) {
	log.debug("setDoorState called with state ${newState}")
	if(newState == "open") {
        sendEvent(name: "door", value: "open", isStateChange: true)
    } else if(newState == "closed") {
        sendEvent(name: "door", value: "closed", isStateChange: true)
    } else {  //Unknown....
    	state.door = "unknown"
    }
}

def changeSwitchState(newState) {

	log.trace "Received update that this switch is now $newState"
	switch(newState) {
    	case 1:
			sendEvent(name: "switch", value: "open")
            break;
    	case 0:
        	sendEvent(name: "switch", value: "closed")
            break;
    }
}
